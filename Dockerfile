# A container to donwload and compile vger
FROM alpine:3.15 AS builder

# Install all dependencies required for compiling vger
RUN apk add gcc musl-dev make git libbsd-dev

# Download vger
ARG VGER_VERSION=1.08
RUN git clone --depth 1 --branch ${VGER_VERSION} https://tildegit.org/solene/vger.git

# Compile vger to a static binary which we can copy around
RUN cd /vger \
  && ls -l \
  && make CFLAGS='-O2 -s -static'

# A container to test the compiled binary running in the final container base image
FROM busybox:musl AS test

# Copy the static binary to test
COPY --from=builder /vger/vger /
# Copy tests
COPY --from=builder /vger/tests /tests

RUN cd /tests && sh test.sh

# The final container
FROM busybox:musl

# Copy the vger static binary
COPY --from=builder /vger/vger /

# Use vger test data a default data
COPY --from=builder /vger/tests/var/gemini /var/gemini

# Shell script to fix permission of the data directory and launch syslog and tcpsvd
COPY /entrypoint.sh /

# The UID and GID of the gemini user running vger and owning its data direcotry
ENV PUID=1000
ENV PGID=1000

ENTRYPOINT ["sh" , "/entrypoint.sh"]
